var turn = 0;
var completes = 0;
var game;//Canvas
var context;//Canvas Context
var currentItem;
var data = ['a','b','c','d','e','f','a','b','c','d','e','f'];
var score = 0;
var bonus = 0;
var bonusTimer;

$(function()
{
	//init game
	initGrid();
});

function updateScore(newScore)
{
	$('#score').html((newScore / 2) * 5);
	
	if(newScore == 0) {
		updateBonus(0);
	} else if(newScore == score) {
		decayBonus();
	} else {
		updateBonus(bonus + 1);
		checkReward();
	}	
	score = newScore;
	if(completes == GRID_SIZE)
	{
		winner();
	}
}

function updateBonus(newBonus)
{
	bonus = newBonus;
	$('#bonus').html(bonus);
	if(bonusTimer != null)
		clearTimeout(bonusTimer);
	bonusTimer = setTimeout(collectBonus, 2000);
}

function decayBonus()
{
	var newBonus = bonus - 1;
	if(newBonus < 1)//uint
		newBonus = 0;
	updateBonus(newBonus);
}

function collectBonus()
{
	var finalBonus = bonus;
	updateBonus(0);
	updateScore(score + finalBonus);
}

function checkReward()
{
	var rd = Boolean(randomRange(0,1));
	if(rd)
	{
	}
}

function winner()
{
	if(bonusTimer != null)
		clearTimeout(bonusTimer);
	collectBonus();
	$('#restart').html('Play Again!');
}

function restart()
{
	$('#restart').html('Restart');
	turn = 0;
	updateScore(0);
	clearGrid();
}

function random(x)
{
	    return Math.floor(x * (Math.random() % 1));
}

function randomRange(min, max)
{
  return min + random(max - min + 1);
}